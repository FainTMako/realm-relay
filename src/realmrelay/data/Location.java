package realmrelay.data;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


public class Location extends Point.Float implements IData {
	
	@Override
	public void parseFromInput(DataInput in) throws IOException {
		this.x = in.readFloat();
                this.y = in.readFloat();
	}
        
	
	@Override
	public void writeToOutput(DataOutput out) throws IOException {
		out.writeFloat(this.x);
		out.writeFloat(this.y);
	}
        
        public double getAngle(Location target)
        {
            double xDiff = target.x - x;
            double yDiff = target.y - y;
            return Math.atan2(yDiff, xDiff);
        }
	
	public float distanceSquaredTo(Location location) {
		float dx = location.x - this.x;
		float dy = location.y - this.y;
		return dx * dx + dy * dy;
	}
	
	public float distanceTo(Location location) {
		return (float) Math.sqrt(this.distanceSquaredTo(location));
	}
        
        public Location()
        {
            
        }
        
        public Location(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

    @Override
    public void setLocation(double x, double y)
    {
    }

}
