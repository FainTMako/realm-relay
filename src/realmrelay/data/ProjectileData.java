package realmrelay.data;

public class ProjectileData {
	
	public String objectId = ""; // element ObjectId
	public float speed = 0; // element Speed
	public int maxDamage = 0; // element MaxDamage
	public int minDamage = 0; // element MinDamage
	public int lifetimeMS = 0; // element LifetimeMS
        public boolean wavy = false;
        public boolean parametric = false;
        public boolean boomerang = false;
        public double amplitude = 0;
        public double frequency = 1;
        public double magnitude = 3;

}
