package realmrelay;

import java.net.Socket;

import realmrelay.crypto.RC4;
import realmrelay.script.ScriptManager;


public class User {
    
        public final static String incomingIoCipher = "72c5583cafb6818995cdd74b80";
        public  final static String outgoingIoCipher = "311f80691451c71d09a13a2a6e";
	private static final String rc4key0 =  outgoingIoCipher;
	private static final String rc4key1 = incomingIoCipher;
	
        private static final int bufferLength = 65537;
	
	public final byte[] localBuffer = new byte[bufferLength];
	public int localBufferIndex = 0;
	public final RC4 localRecvRC4 = new RC4(rc4key0);
	public final RC4 localSendRC4 = new RC4(rc4key1);
	public byte[] remoteBuffer = new byte[bufferLength];
	public int remoteBufferIndex = 0;
	public final RC4 remoteRecvRC4 = new RC4(rc4key1);
	public final RC4 remoteSendRC4 = new RC4(rc4key0);
	public final Socket localSocket;
	public Socket remoteSocket = null;
	public final ScriptManager scriptManager = new ScriptManager(this);
	public long localNoDataTime = System.currentTimeMillis();
	public long remoteNoDataTime = System.currentTimeMillis();
	
	public User(Socket localSocket) {
		if (localSocket == null) {
			throw new NullPointerException();
		}
		this.localSocket = localSocket;
	}
	
	public void disconnect() {
		if (this.remoteSocket != null) {
			try {
				this.remoteSocket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.remoteSocket = null;
			this.scriptManager.trigger("onDisconnect");
		}
	}
	
	public void kick() {
		this.disconnect();
		try {
			this.localSocket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
